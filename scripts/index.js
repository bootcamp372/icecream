window.onload = init;

function init () {
    const coneRadio = document.getElementById("coneRadio");
    const cupRadio = document.getElementById("cupRadio");
    const submitBtn = document.getElementById("submitOrder");

    coneRadio.onclick = hideToppings;
    cupRadio.onclick = hideToppings;
    submitBtn.onclick = onSubmit;
}

function hideToppings () {
    const toppingsDiv = document.getElementById("toppingsDiv");
    const coneRadio = document.getElementById("coneRadio");

    if (coneRadio.checked) {
        toppingsDiv.style.display = "none";
    }
    else {
        toppingsDiv.style.display = "block";
    }
}

function getToppingOptions () {
    const toppingsOptionsInputs = document.querySelectorAll("input[type=checkbox]");
    let toppingsTotal = 0;


    if (cupRadio.checked) {
        toppingsOptionsInputs.forEach(function (item, index) {
            if (item.checked) {
               toppingsTotal += Number(item.value);
            }
       });
    }
    return Number(toppingsTotal.toFixed(2));
}

function onSubmit () {
    const basePricePara = document.getElementById("basePricePara");
    const taxPara = document.getElementById("taxPara");
    const totalPara = document.getElementById("totalPara");

    const baseScoop = 2.25;
    const addScoop = 1.25;

    let numOfScoops = document.getElementById("numOfScoops").value;
    let scoopsTotal = Number((baseScoop + (addScoop * (numOfScoops - 1))).toFixed(2));
    let toppingsTotal = getToppingOptions();

    let baseCost = Number((scoopsTotal + toppingsTotal).toFixed(2));
    let tax = Number((baseCost * 0.06).toFixed(2));

    let totalCost = baseCost + tax;

    basePricePara.innerHTML = baseCost;
    taxPara.innerHTML = tax;
    totalPara.innerHTML = totalCost;
    debugger;

    showModal();


    return false;
}

function showModal() {
    debugger;
    const confirmationModal = new bootstrap.Modal(document.getElementById("confirmationModal"), {});
    confirmationModal.show();
}